<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Driver;
use App\Driver_location;
use App\Authenticator;

class DriverController extends BaseController
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    private $authenticator;

    public function __construct(Authenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    public function login(Request $request)
    {


        $credentials = array_values($request->only('phone', 'password', 'provider'));

        if (! $driver = $this->authenticator->attempt(...$credentials)) {
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised'], 403);
        } else {

            $success['token'] =  $driver->createToken('LMK')->accessToken;
            $success['firstname'] =  $driver->firstname;
            $success['lastname'] =  $driver->lastname;

            $driver = Driver::find($driver->id);
            $driver->fcm_token = $request->input('fcm_token');
            $driver->app_os = $request->input('app_os');
            $driver->app_version = $request->input('app_version');
            $driver->save();

            return $this->sendResponse($success, 'Driver login successfully.');
        }
    }

    /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Customer info api
     *
     * @return \Illuminate\Http\Response
     */
    public function info(Request $request)
    {
        return response()->json($request->user());
    }

    public function location(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        Driver_location::create(['driver_id' => $request->user()->id, 'latitude' => $request->input('latitude'), 'longitude' => $request->input('longitude')]);
        return $this->sendResponse([], 'Driver location successfully.');
    }
}

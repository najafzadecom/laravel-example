<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Customer;
use App\Sms;
use App\Sms_code;
use Illuminate\Support\Facades\Auth;
use Validator;
use Avatar;
use Storage;
use Illuminate\Support\Facades\DB;


class CustomerController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:users'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $sms_code = rand(1111, 9999);
        $sms_message = 'Your activation code: '.$sms_code;

        $input = $request->all();
        $input['sms_code'] = $sms_code;
        $user = Customer::create($input);
        Sms::send($sms_message, str_replace('+', '', $user->phone));
        Sms::create(['user_id' => $user->id, 'phone' => $user->phone, 'message' => $sms_message, 'source' => 'register']);

        Sms_code::create(['user_id' => $user->id, 'phone' => $user->phone, 'code' => $sms_code, 'source' => 'register']);

        $success['token'] =  $user->createToken('LMK')->accessToken;
        $success['sms_code'] =  $sms_code;
        $avatar = Avatar::create($user->firstname)->getImageObject()->encode('png');
        Storage::put('public/avatars/'.$user->id.'/avatar.png', (string) $avatar);

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Activate api
     *
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sms_code' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = Customer::where('id', $request->user()->id)->where('sms_code', $request->input('sms_code'))->update(['status' => 1]);

        if(!$user) {
            return $this->sendError('Activation failed.', []);
        }

        $success['message'] = 'OK';

        return $this->sendResponse($success, 'User activated successfully.');

    }

    /**
     * Complete api
     *
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email,' .$request->user()->id,
            'gender' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user =  Customer::find($request->user()->id);

        $input = $request->all();
        $user->firstname = $input['firstname'];
        $user->lastname = $input['lastname'];
        $user->email = $input['email'];
        $user->gender = $input['gender'];
        $user->password = bcrypt($input['password']);
        $user->save();

        $success['message'] = 'OK';

        return $this->sendResponse($success, 'User profile data updated successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::guard('customer')->attempt(['phone' => $request->phone, 'password' => $request->password, 'status' => 1, 'blocked' => 0])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('LMK')-> accessToken;
            $success['firstname'] =  $user->firstname;
            $success['lastname'] =  $user->lastname;

            // Update User App Data
            $user = Customer::find($user->id);
            $user->fcm_token = $request->input('fcm_token');
            $user->app_os = $request->input('app_os');
            $user->app_version = $request->input('app_version');
            $user->save();

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Customer info api
     *
     * @return \Illuminate\Http\Response
     */
    public function info(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Forget password api
     *
     * @return \Illuminate\Http\Response
     */
    public function forget_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = Customer::where('phone', $request->input('phone'))->first();

        if(!$user) {
            return $this->sendError('User not found.', []);
        }

        $sms_code = rand(1111, 9999);
        $sms_message = 'Your activation code: '.$sms_code;
        $user->sms_code = $sms_code;
        $user->save();

        Sms::create(['user_id' => $user->id, 'phone' => $user->phone, 'message' => $sms_message, 'source' => 'forget_password']);
        Sms_code::create(['user_id' => $user->id, 'phone' => $user->phone, 'code' => $sms_code, 'source' => 'forget_password']);

        $success['sms_code'] = $sms_code;
        $success['token'] =  $user->createToken('LMK')->accessToken;

        return $this->sendResponse($success, 'User has been sent sms code successfully.');
    }

    /**
     * Change password api
     *
     * @return \Illuminate\Http\Response
     */
    public function change_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = Customer::find($request->user()->id)->first();

        $input = $request->all();
        $user->password = bcrypt($input['password']);
        $user->save();

        $success['token'] = $user->createToken('LMK')->accessToken;

        return $this->sendResponse($success, 'User password updated successfully.');
    }
}

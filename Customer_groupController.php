<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Customer_group;

class Customer_groupController extends BaseController
{

    public function __construct()
    {
        $this->middleware('permission:customer_group-list|customer_group-create|customer_group-edit|customer_group-delete', ['only' => ['index','show']]);
        $this->middleware('permission:customer_group-create', ['only' => ['create','store']]);
        $this->middleware('permission:customer_group-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:customer_group-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $column     = request()->get('column', 'id');
        $order      = request()->get('order', 'DESC');
        $d_order    = (request()->get('order') == 'ASC') ? 'DESC' : 'ASC';
        $per_page   = request()->get('per_page', 10);

        $customer_groups = Customer_group::orderBy($column, $order)->latest()->paginate($per_page);

        return view('administration.customer_group.index', compact('customer_groups', 'd_order'))
            ->with('i', (request()->input('page', 1) - 1) * $per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.customer_group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'status' => 'required|in:0,1'
        ]);

        Customer_group::create($request->all());

        return redirect()->route('customer_group.index')
            ->with('success', trans('administration/messages.create_success_message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function show(Customer_group $customer_group)
    {
        return view('administration.customer_group.show',compact('customer_group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer_group $customer_group)
    {
        return view('administration.customer_group.edit',compact('customer_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer_group $customer_group)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'status' => 'required|in:0,1'
        ]);

        $customer_group->update($request->all());

        return redirect()->route('customer_group.index')
            ->with('success', trans('administration/messages.edit_success_message'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer_group $customer_group)
    {
        $customer_group->delete();

        return redirect()->route('customer_group.index')
            ->with('success', trans('administration/messages.delete_success_message'));
    }
}
